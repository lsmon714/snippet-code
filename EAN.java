package com.frommap.ws.request;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;

import com.datastax.driver.core.utils.UUIDs;
import com.frommap.cassandra.dao.EanHotelDetailDao;
import com.frommap.cassandra.dao.PlacesDao;
import com.frommap.util.Mashery;
import com.frommap.ws.bean.EanHotel;
import com.frommap.ws.bean.EanHotelDetail;
import com.frommap.ws.bean.Place;

/**
 * Expedia Affiliate Network API Connection
 * 
 * @author lsmon
 * 
 */
public class EAN {
	private String _url;
	private final String base_url = "http://api.eancdn.com/ean-services/rs/hotel/v3/list?";
	private final int cid = 55505;
	private final String apiKey = "xxxxxxxxxxxxxxxxxxx";
	private final int minorRev = 4;
	private final String supplierCacheTolerance="MED_ENHANCED";
	
	private String customerSessionId;
	private String customerUserAgent;
	private String customerIpAddress;
	private String stateProvinceCode;
	private String countryCode;
	private String city;
	private String sig;

	public String getCustomerSessionId() {
		return customerSessionId;
	}

	public void setCustomerSessionId(String customerSessionId) {
		this.customerSessionId = customerSessionId;
	}

	public String getCustomerUserAgent() {
		return customerUserAgent;
	}

	public void setCustomerUserAgent(String customerUserAgent) {
		this.customerUserAgent = customerUserAgent;
	}

	public String getCustomerIpAddress() {
		return customerIpAddress;
	}

	public void setCustomerIpAddress(String customerIpAddress) {
		this.customerIpAddress = customerIpAddress;
	}

	public String getStateProvinceCode() {
		return stateProvinceCode;
	}

	public void setStateProvinceCode(String stateProvinceCode) {
		this.stateProvinceCode = stateProvinceCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	public EAN(){
		_url = "localhost";
		try {
			sig = Mashery.sig(apiKey);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	public EAN(String clusterAddress) {
		_url = clusterAddress;
		try {
			sig = Mashery.sig(apiKey);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	public List<EanHotel> searchHotelsOnExpedia(){
		List<EanHotel> results = new LinkedList<>();
		
		String ean_url = base_url;
		ean_url += "minorRev=" + this.minorRev + "&";
		ean_url += "cid=" + this.cid + "&";
		ean_url += "apiKey=" + this.apiKey + "&";
		ean_url += "locale=en_US" + "&";
		ean_url += "currencyCode=USD" + "&";
		ean_url += "city=" + this.city + "&";
		ean_url += "stateProvinceCode=" + this.stateProvinceCode + "&";
		ean_url += "countryCode=" + this.countryCode + "&";
		ean_url += "suplierCacheTolerance=" + this.supplierCacheTolerance;// + "&";
		ean_url += "customerIpAddress=" + this.customerIpAddress + "&";
		ean_url += "customerSessionId=" + this.customerSessionId + "&";
		ean_url += "sig=" + this.sig + "&";
		ean_url += "customerUserAgent=" + this.customerUserAgent;
		ean_url = ean_url.replaceAll(" ", "%20");
		
		try {
			JsonFactory factory= new JsonFactory();
			JsonParser parser = factory.createJsonParser(new URL(ean_url));
			while (parser.nextToken() != JsonToken.END_OBJECT) {
				String fieldname = parser.getCurrentName();
				if (fieldname != null && fieldname.equals("HotelSummary")) {
					if (parser.nextToken() == JsonToken.START_ARRAY) {
						while(parser.nextToken() != JsonToken.END_ARRAY){
							fieldname = parser.getCurrentName();
							EanHotel hotel = new EanHotel();
							Place p = new Place();
							EanHotelDetail ean = new EanHotelDetail();
							p.setCategory("hotel");
							while(parser.nextToken() != JsonToken.END_OBJECT) {
								String str = fieldname = parser.getCurrentName();
								
								if (str != null) {
									switch(str){
									case "address1":
										p.setAddr(getValue(parser));
										break;
									case "address2":
										p.setAddr(p.getAddr() + " " + getValue(parser));
										break;
									case "highRate":
										ean.setHightRate(Double.parseDouble(getValue(parser)));
										break;
									case "hotelRating":
										ean.setHotelRating(Double.parseDouble(getValue(parser)));
										break;
									case "latitude":
										p.setLat(Double.parseDouble(getValue(parser)));
										break;
									case "longitude":
										p.setLng(Double.parseDouble(getValue(parser)));
										break;
									case "lowRate":
										ean.setLowRate(Double.parseDouble(getValue(parser)));
										break;
									case "name":
										p.setName(getValue(parser));
										break;
									case "postalCode":
										ean.setPostalCode(getValue(parser));
										break;
									case "rateCurrencyCode":
										ean.setRateCurrencyCode(getValue(parser));
										break;
									case "deepLink":
										ean.setDeepLink(getValue(parser).replaceAll("(&amp;cid=)[^&]*(&amp;)","$1455207$2"));
										break;
									case "airportCode":
										ean.setAirportCode(getValue(parser));
										break;
									case "shortDescription":
										ean.setShortDescription(getValue(parser));
										break;
									}
								}
							}
							PlacesDao d = new PlacesDao(_url);
							d.completeRecord(p);
							UUID id = (p.getId() == null)? UUIDs.timeBased() : p.getId();
							d.insertPlaces(p, id);
							d.close();
							EanHotelDetailDao ed = new EanHotelDetailDao(_url);
							ean.setPlaceId(id);
							ed.insertRecord(ean);
							ed.close();
							hotel.setHotel(p);
							hotel.setDetail(ean);
							results.add(hotel);
						}
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return results;
	}
	
	private String getValue(JsonParser parser) throws JsonParseException, IOException {
		parser.nextToken();
		return parser.getText();
	}

}